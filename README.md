## Description
1. Data analysis on Google's data on job's execution in GCP.
2. Implementation of a stream processing application in Python and another one in Scala.


## Technologies Used
- PySpark
- Kafka
- Scala
- Python

## Requirements

To install project dependencies, in project directory run:

```bash
./requirements.sh
```

If you don't want to reinstall some of the dependencies, you need to comment the corresponding line in the script.
However, this script also adds environment variables to your ~/.bashrc so you should ensure these variables are set to make the project work properly.

After that data for each table should be put in its appropriate folder.

## Project Structure

In the root directory, you will find:

- **data**: where the data (google dataset) should be stored.
- **res**: where the results of the analyses will be stored.
- **scripts**: some useful scripts detailled in the *Miscellaneous commands* section.
- **src**: the source for the batch-processing analyses (and a few utilities (e.g. schema class, application settings, ...) for both batch and stream processing).
- **streaming**: the python source for the replayer and the python streaming app.
- **streaming_scala**: the scala streaming app.


## Topics

We have three topics:

- **event**: Used for the computation of the distribution of the machines according to their CPU capacity
- **usage**: Used for the computation of the most frequent CPU consumptions
- **mem**: Unused (created for the analysis on memory consumption that we did not finish)


## Miscellaneous commands

Here are a few useful commands you can use from the project directory.

Run a batch processing analysis:

```bash
python3 src/main.py -q <question-id>
```

To run perfomance evaluations:
```bash
python3 src/main.py -e <evaluation-id>
```

Start kafka (zookeeper and kafka broker):

```bash
./scripts/start_kafka.sh
```

Create kafka topics ("event", "usage" and "mem"):

```bash
./kafka_topics.sh
```

Start the replayer (kafka producer):

```bash
python3 ./streaming/replay.py -t <topic(s)>
```

Run the "event" consumer:

```bash
./streaming/start_consumer.sh event
```


Run the "usage" consumer:
Inside `streaming_scala` directory compile the app using:
```bash
sbt package
```
Then run
```bash
./streaming_scala/start_consumer.sh
```


## Versions

Spark version:
`spark-3.3.0-bin-hadoop2.7`

Kafka version:
`kafka_2.12-2.7.0`

Spark streaming:
`spark-streaming-kafka-0-10_2.12:3.0.1`


## Developers

J.I and me :)
