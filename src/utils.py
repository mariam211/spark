import os, sys
from pyspark import SparkContext
import time

# Print an RDD
#       name:   name of the RDD to be printed
#       rdd:    the RDD object
#       nb:             number of values to be taken
#       sort:   id of the value to be sorted with
#       asc:    ascending (True/False)
def print_subset(name, rdd, nb=5, sort=-1, ascending=True):
        print("A few examples of "+str(name)+":")
        if sort<0:
                sorted_set = rdd.sortBy(lambda x: x)
        else:
                sorted_set = rdd.sortBy(lambda x: x[sort], ascending)

        if nb<0:
                final_set = sorted_set.collect()
        else:
                final_set = sorted_set.take(nb)

        for elem in final_set:
                print("\t- " + str(elem))

