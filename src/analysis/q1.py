from typing import Iterable, Optional
from pyspark import StorageLevel

import settings
from plot.output import *

from utils import *

def q1():
	print("Q1. What is the distribution of the machines according to their CPU capacity?")



	###############
	# COMPUTATION #
	###############

	# read the input file into an RDD[String]
	# Note that you need to have downloaded machine_events/part-00000-of-00001.csv
	# Tod od so, you can run:
	#	> gsutil cp gs://clusterdata-2011-2/machine_events/part-00000-of-00001.csv.gz data/machine_events/
	#	> cd data/machine_events/ ; gzip -d part-00000-of-00001.csv.gz
	whole_file = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"],"machine_events/"), minPartitions=4)

	id_field_no = settings.schema.get_field_nb("machine_events/part-00000-of-00001.csv.gz", "machine ID")
	cpus_field_no = settings.schema.get_field_nb("machine_events/part-00000-of-00001.csv.gz", "CPUs")

	res = whole_file.map(lambda x: (x.split(",")))
	res.cache()

	res = res.map(lambda x: (x[id_field_no], x[cpus_field_no]))\
		.distinct()\
		.map(lambda x: (x[1], 1))\
		.filter(lambda x: x[0]!='')\
		.reduceByKey(lambda x,y: x+y)\
                .map(lambda x: (float(x[0]), x[1]))\
                .sortBy(lambda x: x[0])



	##########
	# OUTPUT #
	##########

	data = [
		{
			"label": "Q1",
			"data": res.collect()
		}
	]
	labels = ["nb CPUs","nb machines"]

	plot(data, labels, "q1")
