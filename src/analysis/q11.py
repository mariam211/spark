import os
from pyspark import StorageLevel
import settings


def q11():
    print("Q11. What's the average number of times a task gets descheduled?")

    task_events_filename = "task_events/part-?????-of-?????.csv.gz"
    task_events = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"],"task_events/"), minPartitions=4)
    task_events = task_events.map(lambda x : x.split(','))
    task_events.persist(StorageLevel.DISK_ONLY)

    time_index = settings.schema.get_column_for_file(task_events_filename, "time")
    job_id_index = settings.schema.get_column_for_file(task_events_filename, "job ID")
    task_index = settings.schema.get_column_for_file(task_events_filename, "task index")
    event_type_index = settings.schema.get_column_for_file(task_events_filename, "event type")

    task_event_times_change = task_events.filter(lambda x: int(x[event_type_index])==2 or int(x[event_type_index])==3)\
        .map(lambda x: ((int(x[job_id_index]), int(x[task_index])), int(x[time_index])))\
        .distinct()\
        .groupByKey().mapValues(len)\
        .sortBy(lambda x: x[1], ascending=False)
    
    nb_changes = task_event_times_change.filter(lambda x: x[1]>0).count()
    total_task_change = task_event_times_change.map(lambda x: x[1]).reduce(lambda x, y: x+y)

    print("Maximum number of times a task got desceduled", task_event_times_change.take(1)[0][1])
    print("Number of tasks descheduled", nb_changes)
    print("Total number of deschedulings", total_task_change)
    print("Average number of descedulings {:.3f}".format(total_task_change/nb_changes))
