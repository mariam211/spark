import os
from pyspark import StorageLevel
import settings


def q10():
    print("Q10. First data analysis to get helpful general information")
    machine_events_filename = "machine_events/part-00000-of-00001.csv.gz"
    machine_events = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"],"machine_events/"), minPartitions=4)
    machine_events = machine_events.map(lambda x : x.split(','))
    machine_events.cache()

    id_index = settings.schema.get_column_for_file(machine_events_filename, "machine ID")
    type_index = settings.schema.get_column_for_file(machine_events_filename, "event type")
    time_index = settings.schema.get_column_for_file(machine_events_filename, "time")

    total_machines =  machine_events.map(lambda x: x[id_index]).distinct().count()
    removes = machine_events.filter(lambda x: int(x[type_index])==1)
    adds = machine_events.filter(lambda x: int(x[type_index])==0)
    updates = machine_events.filter(lambda x: int(x[type_index])==2)\
                            .map(lambda x: x[id_index]).distinct()

    print("Number of machines added at start: ", machine_events.filter(lambda x: int(x[type_index])==0 and int(x[time_index])==0).map(lambda x: x[id_index]).distinct().count())
    print("Total number of machines: ", total_machines)
    print("Number of updated machines: ", updates.count())
    print("Number of removes: ",removes.count())
    print("Number of additions: ", adds.count())
