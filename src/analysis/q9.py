import os
from pyspark import StorageLevel
import settings


def q9():
    print("Q9. Do same tasks change their ressource usage over time? if yes then how often?")

    task_usage_filename = "task_usage/part-?????-of-?????.csv.gz"

    job_id_index_usage = settings.schema.get_column_for_file(task_usage_filename, "job ID")
    task_index_usage = settings.schema.get_column_for_file(task_usage_filename, "task index")
    cpu_usage_index = settings.schema.get_column_for_file(task_usage_filename, "sampled CPU usage")
    max_mem_index = settings.schema.get_column_for_file(task_usage_filename, "maximum memory usage")
    disk_usage_index = settings.schema.get_column_for_file(task_usage_filename, "local disk space usage")

    task_usage = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"], "task_usage/"), minPartitions=8)
    task_usage = task_usage.map(lambda x : x.split(','))	
    task_usage.persist(StorageLevel.DISK_ONLY)


    task_cpu_usage_change = task_usage.map(lambda x: ((int(x[job_id_index_usage]), int(x[task_index_usage])), float(x[cpu_usage_index])))\
        .distinct()\
        .groupByKey().mapValues(len)\
        .sortBy(lambda x: x[1], ascending=False)

    task_mem_usage_change = task_usage.map(lambda x: ((int(x[job_id_index_usage]), int(x[task_index_usage])), float(x[max_mem_index])))\
        .distinct()\
        .groupByKey().mapValues(len)\
        .sortBy(lambda x: x[1], ascending=False)

    task_disk_usage_change = task_usage.map(lambda x: ((int(x[job_id_index_usage]), int(x[task_index_usage])), float(x[disk_usage_index])))\
        .distinct()\
        .groupByKey().mapValues(len)\
        .sortBy(lambda x: x[1], ascending=False)


    print("Maximum number of times a task has used different ressources:")
    print("For CPU:", task_cpu_usage_change.take(1)[0][1])
    print("For Memory:", task_mem_usage_change.take(1)[0][1])
    print("For Disk:", task_disk_usage_change.take(1)[0][1])
