import os
from pyspark import StorageLevel
from operator import add

import settings

def q7():
	print("Q7. Can we observe correlations between peaks of high resource consumption on some machines and task eviction events?")

	task_events_filename = "task_events/part-?????-of-?????.csv.gz"
	task_events = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"],"task_events/"), minPartitions=4)
	task_events = task_events.map(lambda x : x.split(','))
	task_events.persist(StorageLevel.DISK_ONLY)
	
	job_id_index = settings.schema.get_column_for_file(task_events_filename, "job ID")
	task_index = settings.schema.get_column_for_file(task_events_filename, "task index")
	time_index = settings.schema.get_column_for_file(task_events_filename, "time")
	event_type_index = settings.schema.get_column_for_file(task_events_filename, "event type")

	task_usage_filename = "task_usage/part-?????-of-?????.csv.gz"

	job_id_index_usage = settings.schema.get_column_for_file(task_usage_filename, "job ID")
	task_index_usage = settings.schema.get_column_for_file(task_usage_filename, "task index")
	cpu_usage_index = settings.schema.get_column_for_file(task_usage_filename, "sampled CPU usage")
	max_mem_index = settings.schema.get_column_for_file(task_usage_filename, "maximum memory usage")
	disk_usage_index = settings.schema.get_column_for_file(task_usage_filename, "local disk space usage")
	starttime_index = settings.schema.get_column_for_file(task_usage_filename, "start time")
	endtime_index = settings.schema.get_column_for_file(task_usage_filename, "end time")

	task_usage = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"], "task_usage/"), minPartitions=4)
	task_usage = task_usage.map(lambda x : x.split(','))
	task_usage.persist(StorageLevel.DISK_ONLY)

	evicted_tasks = task_events.filter(lambda x: int(x[event_type_index])==2)
	
	usage_end_time = int(task_usage.sortBy(lambda x: int(x[endtime_index]), ascending=False).take(1)[0][endtime_index])
	events_end_time = int(evicted_tasks.sortBy(lambda x: int(x[time_index]), ascending=False).take(1)[0][time_index])
	#to take all events and usages into account, end_time should be the last seen time
	end_time = max(events_end_time, usage_end_time)

	evicted_tasks = evicted_tasks.map(lambda x: ((int(x[job_id_index]), int(x[task_index])), int(x[time_index])))
	#average over ressource usages from same task at same start time
	task_usages = task_usage.map(lambda x: ((int(x[job_id_index_usage]), int(x[task_index_usage]), int(x[starttime_index])), float(x[cpu_usage_index])+float(x[max_mem_index])+float(x[disk_usage_index])))\
							.groupByKey().mapValues(lambda x: sum(x)/len(x))

	#input format ((jobid, task idx, start time), usages))
	#map output format ((jobid, task idx), (start time, usages))
	task_usages = task_usages.map(lambda x: ((x[0][0], x[0][1]), (x[0][2], x[1])))
	#add dummy usages so that the last usages before end_time are counted too
	missing_usages = task_usages.map(lambda x: (x[0], (end_time, 0)))
	full_usages = task_usages.union(missing_usages)

	#format after self join ((jobid, task idx), ((start time, usages), (other start time, other usages)))
	# ((jobid, task idx, (start time, next start time), usages), times diff)))
	# ((jobid, task idx), ((start time, next start time), usages))
	task_time_usages = full_usages.join(full_usages).map(lambda x: ((*x[0], x[1][0][0], x[1][0][1]), x[1][1][0] - x[1][0][0]))\
						.filter(lambda x: x[1]>0).groupByKey().mapValues(min)\
						.map(lambda x: ((x[0][0], x[0][1]), ((x[0][2], x[0][2]+x[1]), x[0][3])))

	#((jobid, task idx), ((start time, next start time), usages))
	#((job_id_index, task_index), evict time)
	#((jobid, task idx), (((start time, next start time), usages), evict time))
	#add tasks with no evictions
	no_eviction_tasks = task_time_usages.map(lambda x: ((*x[0], *x[1]), 0))

	#((jobid, task idx, (start time, next start time), usages), (evict time - start time, next start time - evict time))
	#((jobid, task idx, (start time, next start time), usages), evictions count)
	task_eviction_usage = task_time_usages.join(evicted_tasks).map(lambda x: ((*x[0], *x[1][0]), (x[1][1]-x[1][0][0][0], x[1][0][0][1]-x[1][1])))\
						.filter(lambda x: x[1][0]>0 and x[1][1]>=0).map(lambda x: (x[0], 1))\
						.reduceByKey(add)

	#((jobid, task idx, (start time, next start time), usages), evictions count)
	#keys that are in both rdds their usage will be summed together, so keys with value 0 will correspond to tasks that are not in task_eviction_usage hence they were not evicted
	total_eviction_usages = no_eviction_tasks.union(task_eviction_usage).reduceByKey(add)
	nb_evictions = total_eviction_usages.map(lambda x: x[1]).reduce(add)

	#(usages, total evictions)
	#sum evictions for each usage
	usages_evictions = total_eviction_usages.map(lambda x: (x[0][3], x[1])).reduceByKey(add)
	evictions_mean = usages_evictions.map(lambda x: x[1]).mean()
	usage_mean = usages_evictions.map(lambda x: x[0]).mean()
	print("Total number of evictions:", nb_evictions)
	print("Usage mean:", usage_mean)
	print("Evictions mean:", evictions_mean)

	#(usages, evictions, cond)
	#(cond, avg evictions)
	evictions_by_usage = usages_evictions.map(lambda x: (*x, x[0]>usage_mean)).map(lambda x: (x[2], x[1])).groupByKey().mapValues(lambda x : sum(x)/len(x)).collect()
	
	print("########")
	for t in evictions_by_usage:
		if(t[0]):
			print("Evictions average for tasks with high ressource usage(strictly higher than {:.7f}): {:.7f}".format(usage_mean, t[1]))
		else:
			print("Evictions average for tasks with low ressource usage(bellow or equal to {:.7f}): {:.7f}".format(usage_mean, t[1]))

