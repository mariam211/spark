import os
from pyspark import StorageLevel
import settings


def q8():
    print("Q8. What is the maximum number of times a machine got its memory or CPU changed?")

    machine_events_filename = "machine_events/part-00000-of-00001.csv.gz"
    machine_events = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"],"machine_events/"), minPartitions=4)
    machine_events = machine_events.map(lambda x : x.split(','))
    machine_events.cache()

    id_index = settings.schema.get_column_for_file(machine_events_filename, "machine ID")
    type_index = settings.schema.get_column_for_file(machine_events_filename, "event type")
    cpus_index = settings.schema.get_column_for_file(machine_events_filename, "CPUs")
    mem_index = settings.schema.get_column_for_file(machine_events_filename, "Memory")


    machine_cpus = machine_events.filter(lambda x: int(x[type_index])!=1)\
        .filter(lambda x: bool(x[cpus_index]))\
        .map(lambda x: (int(x[id_index]), float(x[cpus_index])))\
        .distinct()\
        .groupByKey().mapValues(len)\
        .sortBy(lambda x: x[1], ascending=False)

    machine_mem = machine_events.filter(lambda x: int(x[type_index])!=1)\
        .filter(lambda x: bool(x[mem_index]))\
        .map(lambda x: (int(x[id_index]), float(x[mem_index])))\
        .distinct()\
        .groupByKey().mapValues(len)\
        .sortBy(lambda x: x[1], ascending=False)

    print("Maximum number of different changes of CPU per machine:", machine_cpus.take(1)[0][1])
    print("Maximum number of different changes of Memory per machine:", machine_mem.take(1)[0][1])
