from pyspark import StorageLevel
from utils import *

import settings
from plot.output import *


def q3():
	print("Q3. What is the distribution of the number of jobs/tasks per scheduling class?")

	#####################
	# JOBS DISTRIBUTION #
	#####################
	print("\nWorking on Jobs...\n")

	whole_file = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"],"job_events/"), minPartitions=4)

	class_field = settings.schema.get_field_nb("job_events/part-?????-of-?????.csv.gz", "scheduling class")
	id_field = settings.schema.get_field_nb("job_events/part-?????-of-?????.csv.gz", "job ID")

	res_jobs = whole_file.map(lambda x: x.split(","))
	res_jobs.persist(StorageLevel.MEMORY_AND_DISK)

	res_jobs = res_jobs.map(lambda x: (x[id_field], x[class_field]))\
						.distinct()\
						.filter(lambda x: x[1]!="")\
						.map(lambda x: (x[1],1))\
						.reduceByKey(lambda x,y: x+y)\
						.sortBy(lambda x: x[0])\



	######################
	# TASKS DISTRIBUTION #
	######################
	print("\nWorking on Tasks...\n")

	whole_file = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"],"task_events/"), minPartitions=4)

	class_field = settings.schema.get_field_nb("task_events/part-?????-of-?????.csv.gz", "scheduling class")
	id_field = settings.schema.get_field_nb("task_events/part-?????-of-?????.csv.gz", "job ID")
	index_field = settings.schema.get_field_nb("task_events/part-?????-of-?????.csv.gz", "task index")
	# Note that for a task, the ID id the pair (Job ID, task index)

	res_tasks = whole_file.map(lambda x: x.split(","))
	res_tasks.persist(StorageLevel.MEMORY_AND_DISK)

	res_tasks = res_tasks.map(lambda x: (x[id_field], x[index_field], x[class_field]))\
						.distinct()\
						.filter(lambda x: x[2]!="")\
						.map(lambda x: (x[2],1))\
						.reduceByKey(lambda x,y: x+y)\
						.sortBy(lambda x: x[0])\



	##########
	# OUTPUT #
	##########

	data = [
		{
			"label": "Jobs",
			"data": res_jobs.collect()
		},
		{
			"label": "Tasks",
			"data": res_tasks.collect()
		}
	]
	labels = ["Scheduling Class","Nb Jobs/Tasks"]

	outfile_graph = settings.outname if settings.outname!=None else os.path.join(settings.config["ROOT_PATH"], "results/q3")
	output_graph(data, labels, outfile=outfile_graph, multiplots=True, bar=True)

	outfile_csv_jobs = settings.outname+"-jobs" if settings.outname!=None else os.path.join(settings.config["ROOT_PATH"], "results/q3-jobs")
	with open(outfile_csv_jobs+".csv", mode='w') as csv_file:
		output_csv(data[0]["data"], labels, csv_file)

	outfile_csv_tasks = settings.outname+"-tasks" if settings.outname!=None else os.path.join(settings.config["ROOT_PATH"], "results/q3-tasks")
	with open(outfile_csv_tasks+".csv", mode='w') as csv_file:
		output_csv(data[1]["data"], labels, csv_file)

	print("Check "+str(outfile_graph)+".pdf for the graph output")
	print("Check "+str(outfile_csv_jobs)+".csv for the csv output for jobs")
	print("Check "+str(outfile_csv_tasks)+".csv for the csv output for tasks")
