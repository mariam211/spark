import sys
from pyspark import StorageLevel
import time
from operator import add
import os, sys
import settings


def q2():
	print("Q2. What is the percentage of computational power lost due to maintenance (a machine went off line and reconnected later)?")
	machine_events_filename = "machine_events/part-00000-of-00001.csv.gz"
	machine_events = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"], "machine_events/"), minPartitions=4)	
	machine_events = machine_events.map(lambda x : x.split(','))
	machine_events.cache()
	nb_partitions = machine_events.getNumPartitions()

	id_index = settings.schema.get_column_for_file(machine_events_filename, "machine ID")
	time_index = settings.schema.get_column_for_file(machine_events_filename, "time")
	type_index = settings.schema.get_column_for_file(machine_events_filename, "event type")
	cpus_index = settings.schema.get_column_for_file(machine_events_filename, "CPUs")

	#Compute global running time for the whole cluster, which starts from the first event time to last event time
	end_time = int(machine_events.sortBy(lambda x: int(x[time_index]), ascending=False).take(1)[0][time_index])

	#Approximate estimation considering just the CPUs of the removed distinct machines and the CPUs of distinct machines added
	#we take the minimum CPU per machine to know the minimum CPU lost from each machine (here it doesn't change the result since no machine has changed its CPU in this dataset)
	cpus_lost = machine_events.filter(lambda x: int(x[type_index])==1)\
		.map(lambda x: (int(x[id_index]), float(x[cpus_index])))\
		.groupByKey().mapValues(min).map(lambda x: x[1]).reduce(add)

	total_cpus = machine_events.filter(lambda x: int(x[type_index])!=1)\
		.filter(lambda x: bool(x[cpus_index]))\
		.map(lambda x: (int(x[id_index]), float(x[cpus_index])))\
		.groupByKey().mapValues(min).map(lambda x: x[1]).reduce(add)

	print("Minimum cummulative normalized CPUs capacity lost: ", cpus_lost)
	print("Total minimum cummulative normalized CPUs:", total_cpus)
	print("Minimum cummulative normalized lost CPUs percentage: {:2.2%}".format(cpus_lost/total_cpus))

	#get all remove and add events time and CPU capacity(we use CPUs value from remove events since no machine changes its CPUs so it doesn't matter) 
	#removes schema (id, (remove time, cpu))
	removes = machine_events.filter(lambda x: int(x[type_index])==1)\
		.map(lambda x: (int(x[id_index]), (int(x[time_index]), float(x[cpus_index]))))\
		.partitionBy(nb_partitions)
	removes.cache()

	#add schema (id, add time)
	adds = machine_events.filter(lambda x: int(x[type_index])==0)\
		.map(lambda x: (int(x[id_index]), int(x[time_index])))\
		.partitionBy(nb_partitions)
	adds.cache()
	#for the computations to be correct we need to extend remove and add events with removes at the end time and adds at the end time
	#so we get for each machine its last remove event has after it an add event at the end time, so that the duration between the two can be included as lost time
	#and we get for each machine's last add event a remove event at the end time, so that the duration between them can be included as available time
	missing_removes = machine_events.filter(lambda x: (int(x[type_index])==0 or int(x[type_index])==2) and bool(x[cpus_index]))\
		.map(lambda x: ((int(x[id_index]), float(x[cpus_index])), int(x[time_index])))\
		
	missing_removes = missing_removes.groupByKey().mapValues(max).map(lambda x: (x[0][0], (end_time, x[0][1]))).partitionBy(nb_partitions)
	missing_removes.cache()

	missing_adds = removes.map(lambda x: (x[0], end_time))
	full_adds = adds.union(missing_adds)
	full_removes = removes.union(missing_removes)

	#Computation of the total lost CPU for each machine
	#join result format (id, ((remove time, cpu), add time))
	#first map input format ((id, remove time), (add time - remove time)*cpu)
	#we filter out the negative results since we are only interested on durations where remove time is before add time
	#Here the computation is done between each remove event and each add event(per machine)
	#We use the machine id and the remove times as key so that we can have one duration per machine and remove time, then we apply min on computed values in order to get smallest duration
	#corresponding to the closest add event after a remove event
	rm_durations = removes.join(full_adds).map(lambda x: ((x[0], x[1][0][0]), (x[1][1] - x[1][0][0])*x[1][0][1]))
	rm_durations = rm_durations.filter(lambda x: x[1]>0)\
		.groupByKey().mapValues(min)\
		.map(lambda x: (x[0][0], x[1])).partitionBy(nb_partitions)
	rm_durations.cache()
	rm_durations = rm_durations.reduceByKey(add)
	#first map input format ((id, remove time), (remove time - add time)*cpu)
	#we filter out the negative results since we are only interested on durations where add time is before remove time

	add_durations = full_removes.join(adds).map(lambda x: ((x[0], x[1][0][0]), (x[1][0][0] - x[1][1])*x[1][0][1]))
	add_durations = add_durations.filter(lambda x: x[1]>0).groupByKey().mapValues(min)\
					.map(lambda x: (x[0][0], x[1])).partitionBy(nb_partitions)
	add_durations.cache()
	add_durations = add_durations.reduceByKey(add)

	#First method to compute percentage of lost CPUs, we just sum all the durations where machines were available with durations were machines were removed
	total_add_durations = add_durations.map(lambda x: x[1]).reduce(add)
	total_rm_durations = rm_durations.map(lambda x: x[1]).reduce(add)

	print("Total CPUs*available durations ",total_add_durations)
	print("Total CPUs*lost durations",total_rm_durations)
	print("Percentage of lost computational power using first method: {:2.2%}".format(total_rm_durations/(total_add_durations+total_rm_durations)))

	#For the second method we will compute the percentage per each machine and then take the average of all machines
	#join result format (id, (lost time, available time))
	#map input (id, lost time/(lost time + available time))
	percentages = rm_durations.join(add_durations).map(lambda x: (x[0], x[1][0]/(x[1][0]+x[1][1])))
	print("Percentage of lost computational power using second method: {:2.2%}".format(percentages.map(lambda x: x[1]).mean()))


