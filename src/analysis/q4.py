import os
from pyspark import StorageLevel
from operator import add
import settings


def q4():
	print("Q4. Do tasks with a low scheduling class have a higher probability of being evicted?")

	task_events_filename = "task_events/part-?????-of-?????.csv.gz"
	task_events = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"],"task_events/"), minPartitions=4)
	task_events = task_events.map(lambda x : x.split(','))
	task_events.persist(StorageLevel.DISK_ONLY)

	print("Total events:", task_events.count())
	scheduling_class_index = settings.schema.get_column_for_file(task_events_filename, "scheduling class")
	event_type_index = settings.schema.get_column_for_file(task_events_filename, "event type")

	#select only tasks that were evicted
	evicted_tasks = task_events.filter(lambda x: int(x[event_type_index])==2)\
		.map(lambda x: (int(x[scheduling_class_index]), 1))\
		.partitionBy(task_events.getNumPartitions())\
		.persist(StorageLevel.MEMORY_AND_DISK)\
		.reduceByKey(add)

	total_evictions = evicted_tasks.map(lambda x: x[1]).reduce(add)
	#(class, evictions count, percentage)
	evicted_tasks = evicted_tasks.map(lambda x: (x[0], x[1], x[1]/total_evictions)).collect()

	print("Total evictions:", total_evictions)

	for sc_class,_,probability in evicted_tasks:
		print("Evicted tasks' percentage {:2.2%} with scheduling class {}".format(probability, sc_class))
