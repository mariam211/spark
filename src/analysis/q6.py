import os
from pyspark import StorageLevel
import settings


def q6():
    print("Q6. Are the tasks that request the more resources the one that consume the more resources?")

    task_events_filename = "task_events/part-?????-of-?????.csv.gz"
    task_events = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"],"task_events/"), minPartitions=4)	
    task_events = task_events.map(lambda x : x.split(','))
    #we persist to disk since we have a lot of data in task events
    task_events.persist(StorageLevel.DISK_ONLY)

    job_id_index = settings.schema.get_column_for_file(task_events_filename, "job ID")
    task_index = settings.schema.get_column_for_file(task_events_filename, "task index")
    time_index = settings.schema.get_column_for_file(task_events_filename, "time")
    cpu_request_index = settings.schema.get_column_for_file(task_events_filename, "CPU request")
    mem_request_index = settings.schema.get_column_for_file(task_events_filename, "memory request")
    disk_request_index = settings.schema.get_column_for_file(task_events_filename, "disk space request")

    task_usage_filename = "task_usage/part-?????-of-?????.csv.gz"

    job_id_index_usage = settings.schema.get_column_for_file(task_usage_filename, "job ID")
    task_index_usage = settings.schema.get_column_for_file(task_usage_filename, "task index")
    cpu_usage_index = settings.schema.get_column_for_file(task_usage_filename, "sampled CPU usage")
    max_mem_index = settings.schema.get_column_for_file(task_usage_filename, "maximum memory usage")
    disk_usage_index = settings.schema.get_column_for_file(task_usage_filename, "local disk space usage")
    starttime_index = settings.schema.get_column_for_file(task_usage_filename, "start time")

    task_usage = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"], "task_usage/"))	
    task_usage = task_usage.map(lambda x : x.split(','))
    task_usage.persist(StorageLevel.DISK_ONLY)

    ##########
    #For CPU
    ##########
    task_events_cpu_req = task_events.filter(lambda x: bool(x[cpu_request_index]))
    task_usage_cpu = task_usage.filter(lambda x: bool(x[cpu_usage_index]))

    cpu_request = task_events_cpu_req.map(lambda x: ((int(x[job_id_index]), int(x[task_index]), float(x[cpu_request_index])), int(x[time_index])))

    cpu_usage = task_usage_cpu.map(lambda x: ((int(x[job_id_index_usage]), int(x[task_index_usage])), (int(x[starttime_index]), float(x[cpu_usage_index]))))

    #join result format ((job_id, task index, cpu_request), (time, other time))
    #mapped to ((jobid, task idx, cpu request, time), other time - time)
    #then map to ((jobid, task idx), (time, next time), cpu request)
    cpu_request = cpu_request.join(cpu_request).map(lambda x: ((*x[0], x[1][0]), x[1][1]-x[1][0]))\
        .filter(lambda x: x[1]>0).groupByKey().mapValues(min)\
        .map(lambda x: ((x[0][0], x[0][1]), ((x[0][3], x[1]+x[0][3]), x[0][2])))

    #joining these two data sets gives us this data format ((job_id, task_index), (((time, next time), cpu_request), (starttime, cpu_usage)))
    #we are only interested on task usages that have a start time after the task event happened
    #first map to ((job_id, task_index, (time, next time), cpu request), (start time - time, next time - start time), cpu usage) in order to filter out combinations of task requests with task usages before the corresponding request time and after the second request time
    #then map to ((job_id, task_index, time, cpu request), cpu usage) the group by key and for each key select the one with the maximum cpu usage
    task_cpu_request_usage = cpu_request.join(cpu_usage)\
        .map(lambda x: ((*x[0], x[1][0][0], x[1][0][1]), (x[1][1][0]-x[1][0][0][0], x[1][0][0][1]-x[1][1][0]), x[1][1][1]))\
        .filter(lambda x: x[1][0]>=0 and x[1][1]>=0)\
        .map(lambda x: (x[0], x[2])).groupByKey().mapValues(max)
    #we can use max value of each cpu request, take max cpu usage for all tasks with same cpu requests, but since it may give us false results if there are outliers we chose to use the average instead
    #from ((job_id, task_index, time, cpu request), cpu usage) map to (cpu request, cpu usage) then to (cpu request, cpu usage average) 
    cpu_request_usage = task_cpu_request_usage.map(lambda x: (x[0][3], x[1]))\
                        .groupByKey().mapValues(lambda x: sum(x)/len(x))

    #We compute the mean of cpu requests
    cpu_requests_mean = cpu_request_usage.map(lambda x: x[0]).mean()
    #We will categorize the data into 2 groups; one that has cpu requests less or equal to mean and the other one has the cpu requests strictly higher than cpu requests mean
    #from (cpu request, cpu usage average) to (cpu request, cpu usage average, cpu request>cpu_requests_mean) then to (cpu request>cpu_requests_mean, cpu usage average)
    cpu_usage_by_category = cpu_request_usage.map(lambda x: (*x, x[0]>cpu_requests_mean))\
                            .map(lambda x: (x[2], x[1])).groupByKey().mapValues(lambda x : sum(x)/len(x)).collect()

    ###########
    #For Memory
    ###########
    task_events_mem_req = task_events.filter(lambda x: bool(x[mem_request_index]))
    task_usage_mem = task_usage.filter(lambda x: bool(x[max_mem_index])) #may need to use also "canonical memory" field since not all tasks have the max memory field

    mem_request = task_events_mem_req.map(lambda x: ((int(x[job_id_index]), int(x[task_index]), float(x[mem_request_index])), int(x[time_index])))\
    #join result format ((job_id, task index, mem_request), (time, other time))
    #mapped to ((jobid, task idx, mem request, time), other time - time)
    #then map to ((jobid, task idx), (time, next time), mem request)
    mem_request = mem_request.join(mem_request).map(lambda x: ((*x[0], x[1][0]), x[1][1]-x[1][0]))\
        .filter(lambda x: x[1]>0).groupByKey().mapValues(min)\
        .map(lambda x: ((x[0][0], x[0][1]), ((x[0][3], x[1]+x[0][3]), x[0][2])))

    mem_usage = task_usage_mem.map(lambda x: ((int(x[job_id_index_usage]), int(x[task_index_usage])), (int(x[starttime_index]), float(x[max_mem_index]))))

    #((x[job_id_index], x[task_index]), ((x[time_index], x[mem_request_index]), (x[starttime_index], x[mem_usage_index])))
    #we are only interested on task usages that have a start time after the corresponding task event happened
    #first map to ((job_id, task_index, (time, next time), memory request), (start time - time, next time - start time), memory usage) in order to filter out combinations of task requests with task usages before the corresponding request time and after the second request time
    #then map to ((job_id, task_index, (time, next time), memory request), memory usage) then group by key and for each key select the one with the maximum cpu usage
    task_mem_request_usage = mem_request.join(mem_usage).map(lambda x: ((*x[0], x[1][0][0], x[1][0][1]), (x[1][1][0]-x[1][0][0][0], x[1][0][0][1]-x[1][1][0]), x[1][1][1]))\
        .filter(lambda x: x[1][0]>=0 and x[1][1]>=0)\
        .map(lambda x: (x[0], x[2])).groupByKey().mapValues(max)
    #take max mem usage for all tasks with same mem requests
    mem_request_usage = task_mem_request_usage.map(lambda x: (x[0][3], x[1])).groupByKey().mapValues(max)

    #Compute mean of memory requests
    mem_requests_mean = mem_request_usage.map(lambda x: x[0]).mean()
    #We will categorize the data into 2 groups; one that has memory requests less or equal to mean and the other one has the memory requests strictly higher than memory requests mean
    #from (memory request, memory usage average) to (memory request, memory usage average, memory request>mem_requests_mean) then to (mempry request>mem_requests_mean, memory usage average)
    mem_usage_by_category = mem_request_usage.map(lambda x: (*x, x[0]>mem_requests_mean))\
        .map(lambda x: (x[2], x[1]))\
        .groupByKey().mapValues(lambda x : sum(x)/len(x)).collect()


    #############
    #For disk
    #############
    task_events_disk_req = task_events.filter(lambda x: bool(x[disk_request_index]))
    task_usage_disk = task_usage.filter(lambda x: bool(x[disk_usage_index]))

    disk_request = task_events_disk_req.map(lambda x: ((int(x[job_id_index]), int(x[task_index]), float(x[disk_request_index])), int(x[time_index])))
    #join result format ((job_id, task index, disk_request), (time, other time))
    #mapped to ((jobid, task idx, disk request, time), other time - time)
    #then map to ((jobid, task idx), (time, next time), disk request)
    disk_request = disk_request.join(disk_request).map(lambda x: ((*x[0], x[1][0]), x[1][1]-x[1][0]))\
        .filter(lambda x: x[1]>0).groupByKey().mapValues(min)\
        .map(lambda x: ((x[0][0], x[0][1]), ((x[0][3], x[1]+x[0][3]), x[0][2])))

    disk_usage = task_usage_disk.map(lambda x: ((int(x[job_id_index_usage]), int(x[task_index_usage])), (int(x[starttime_index]), float(x[disk_usage_index]))))

    #((x[job_id_index], x[task_index]), ((x[time_index], x[disk_request_index]), (x[starttime_index], x[disk_usage_index])))
    #we are only interested on task usages that have a start time after the corresponding task event happened
    #first map to ((job_id, task_index, (time, next time), disk request), (start time - time, next time - start time), disk usage) in order to filter out combinations of task requests with task usages before the corresponding request time and after the second request time
    #then map to ((job_id, task_index, (time, next time), disk request), disk usage) then group by key and for each key select the one with the maximum cpu usage
    task_disk_request_usage = disk_request.join(disk_usage)\
        .map(lambda x: ((*x[0], x[1][0][0], x[1][0][1]), (x[1][1][0]-x[1][0][0][0], x[1][0][0][1]-x[1][1][0]), x[1][1][1]))\
        .filter(lambda x: x[1][0]>=0 and x[1][1]>=0)\
        .map(lambda x: (x[0], x[2])).groupByKey().mapValues(max)
    #take max disk usage for all tasks with same disk requests
    disk_request_usage = task_disk_request_usage.map(lambda x: (x[0][3], x[1]))\
        .groupByKey().mapValues(max)

    #Compute mean of disk requests
    disk_requests_mean = disk_request_usage.map(lambda x: x[0]).mean()

    #We will categorize the data into 2 groups; one that has disk requests less or equal to mean and the other one has the disk requests strictly higher than disk requests mean
    #from (disk request, disk usage average) to (disk request, disk usage average, disk request>disk_requests_mean) then to (disk request>disk_requests_mean, disk usage average)
    disk_usage_by_category = disk_request_usage.map(lambda x: (*x, x[0]>disk_requests_mean))\
        .map(lambda x: (x[2], x[1]))\
        .groupByKey().mapValues(lambda x : sum(x)/len(x)).collect()
        

    print("#######################")
    print("Ressource usage of CPU:")
    print("#######################")

    for t in cpu_usage_by_category:
        if(t[0]):
            print("CPU usage average for tasks with high ressource request(strictly higher than {:.10f}): {:.10f}".format(cpu_requests_mean, t[1]))
        else:
            print("CPU usage average for tasks with low ressource request(bellow or equal to {:.10f}): {:.10f}".format(cpu_requests_mean, t[1]))


    print("###########################")
    print("Ressource usage of memory:")
    print("###########################")

    for t in mem_usage_by_category:
        if(t[0]):
            print("Memory usage average for tasks with high ressource request(strictly higher than {:.10f}): {:.10f}".format(mem_requests_mean, t[1]))
        else:
            print("Memory usage average for tasks with low ressource request(bellow or equal to {:.10f}): {:.10f}".format(mem_requests_mean, t[1]))


    print("#########################")
    print("Ressource usage of disk:")
    print("#########################")

    for t in disk_usage_by_category:
        if(t[0]):
            print("Disk usage average for tasks with high ressource request(strictly higher than {:.10f}): {:.10f}".format(disk_requests_mean, t[1]))
        else:
            print("Disk usage average for tasks with low ressource request(bellow or equal to {:.10f}): {:.10f}".format(disk_requests_mean, t[1]))
