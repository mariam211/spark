from pyspark import StorageLevel

from utils import *
from plot.output import *
import settings

def q5():
	print("Q5. In general, do tasks from the same job run on the same machine?")

	# We will count the number of machines used per job



	###############
	# COMPUTATION #
	###############

	whole_file = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"],"task_events/"), minPartitions=4)

	job_id = settings.schema.get_field_nb("task_events/part-?????-of-?????.csv.gz", "job ID")
	task_ind = settings.schema.get_field_nb("task_events/part-?????-of-?????.csv.gz", "task index")
	machine_id = settings.schema.get_field_nb("task_events/part-?????-of-?????.csv.gz", "machine ID")

	# K = job id, V = number of machines on which its tasks ran
	machines_per_job = whole_file.map(lambda x: x.split(","))
	machines_per_job.persist(StorageLevel.MEMORY_AND_DISK)

	machines_per_job = machines_per_job.map(lambda x: ( x[job_id], x[machine_id] ) )\
		.filter(lambda x: x[1]!=None)\
		.distinct()\
		.map(lambda x: (x[0], 1))\
		.reduceByKey(lambda x,y: x+y)

	jobs_several_machines = machines_per_job\
		.filter(lambda x: x[1]>1)\
		.count()

	jobs_one_machine = machines_per_job\
		.filter(lambda x: x[1]==1)\
		.count()

	jobs_total = machines_per_job.count()

	# K = amount of machines, V = number of jobs which ran its tasks on this amount of machines
	#jobs_per_nb_machines = machines_per_job\
	#	.map(lambda x: (x[1], 1))\
	#	.reduceByKey(lambda x,y: x+y)\
	#	.sortBy(lambda x: x[0])



	##########
	# OUTPUT #
	##########


	print(str(jobs_several_machines) + " jobs have tasks running on multiple machines.")
	print(str(jobs_one_machine) + " jobs only have tasks running on the same machine.")
	percentage = round(jobs_several_machines*100/jobs_total, 2)
	print(str(percentage) + "% of the jobs have tasks running on multiple machines.")

	# Graph
	#other_values = settings.sc\
	#	.parallelize(range(jobs_per_nb_machines.min()[0], jobs_per_nb_machines.max()[0]))\
	#	.map(lambda x: (x, 0))

	#union = jobs_per_nb_machines\
	#	.fullOuterJoin(other_values)\
	#	.map(lambda x: (x[0], max(int(x[1][0] if x[1][0] is not None else 0), int(x[1][1] if x[1][1] is not None else 0))))\
	#	.sortBy(lambda x: x[0])

	data = [
		{
			"label": "Q5",
			"data": [("One", jobs_one_machine), ("Several", jobs_several_machines)]
		}
	]
	labels = ["Nb Machines used","Nb Jobs"]

	plot(data, labels, "q5")

