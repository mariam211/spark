import os
import settings

class Schema:
	def __init__(self, filename):
		self.sc = settings.sc
		self.file = self.sc.textFile(filename)
		first_line = self.file.collect()[0]
		self.cols = first_line.split(',')
		self.entries = self.file.filter(lambda x: not x == first_line).map(lambda x: x.split(","))
		self.entries.cache()

	def get_col(self, name):
		if name in self.cols:
			return self.cols.index(name)
		else:
			return -1

	def list(self, name):
		col = self.get_col(name)
		if col != -1:
			return self.entries.map(lambda x: x[col]).distinct().collect()
		else:
			return None

	def get_field_nb(self, file_pattern, content):
		file_pattern_col = self.get_col("file pattern")
		field_number_col = self.get_col("field number")
		content_col      = self.get_col("content")
		nb = self.entries \
			.filter(lambda x: ((x[file_pattern_col]==file_pattern) and (x[content_col]==content))) \
			.map(lambda x: x[field_number_col]) \
			.collect()[0]
		return int(nb)-1

	def get_columns(self, filename):
		return self.entries.filter(lambda x: filename in x).map(lambda x : x[2]).collect()


	def get_column_for_file(self, filename, column_name):
		file_cols = self.get_columns(filename)

		if column_name in file_cols:
			return file_cols.index(column_name)
		else:
			return -1
