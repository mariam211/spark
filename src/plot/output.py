import matplotlib.pyplot as plt
import csv
import sys, os

import settings


# Output the results to csv files and print graph
#   data: the data to print
#       list of key-value pairs
#   labels: columns labels
#       labels[0] = column 0 label (string)
#       labels[1] = column 1 label (string)
def output(data, labels):
	##############
	# CSV OUTPUT #
	##############

	if settings.csv:
		if settings.outname == None:
			output_csv(data, labels, sys.stdout)
		else:
			with open(settings.outname+".csv", mode='w') as csv_file:
				output_csv(data, labels, csv_file)

	################
	# GRAPH OUTPUT #
	################

	if settings.graph and settings.outname!=None:
		output_graph(data, labels)



def output_csv(data, labels, csv_file):
	writer = csv.DictWriter(csv_file, fieldnames=labels)
	writer.writeheader()
	for row in data: # Iterate over rows
		obj = {}
		for i in range(0, len(labels)): # Iterate over columns
			obj[labels[i]] = row[i]
		writer.writerow(obj)

def output_graph(data, labels, outfile="output/out", multiplots=False, bar=False):
	i=1
	for line in data:
		x = []
		y = []
		for pair in line["data"]:
			x += [pair[0]]
			y += [pair[1]]
		width = 1 / ( len(x) * 4 )
		if multiplots:
			plt.subplot(len(data), 1, i)
		if bar:
			plt.bar(x, y, label=line["label"], width=width)
		else:
			plt.plot(x, y, label=line["label"])
		if multiplots:
			plt.legend()
		i=i+1
	plt.xlabel(labels[0])
	plt.ylabel(labels[1])
	plt.title(settings.outname)
	if not multiplots:
		plt.legend()
	plt.savefig(outfile+".pdf", format='pdf', dpi=1200)


def plot(data, labels, name):
	outfile_graph = settings.outname if settings.outname!=None else os.path.join(settings.config["ROOT_PATH"], "results/"+name)
	output_graph(data, labels, outfile=outfile_graph, bar=True)

	outfile_csv = settings.outname if settings.outname!=None else os.path.join(settings.config["ROOT_PATH"], "results/"+name)
	outfile_csv = outfile_csv + ".csv"
	with open(outfile_csv, mode='w') as csv_file:
		output_csv(data[0]["data"], labels, csv_file)

	print("Check "+str(outfile_graph)+".pdf for the graph output")
	print("Check "+str(outfile_csv)+" for the csv output")
