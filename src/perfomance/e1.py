import os
from operator import add
from pyspark import StorageLevel, SparkContext, SparkConf
import time
from datetime import timedelta
import settings
from .tests import *


def e1():
    print("Application level study: Impact of data persistance on performance")
    machine_events_filename = "machine_events/part-00000-of-00001.csv.gz"
    id_index = settings.schema.get_column_for_file(machine_events_filename, "machine ID")

    machine_events = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"], "machine_events/"), minPartitions=4)
    machine_events = machine_events.map(lambda x : x.split(',')).map(lambda x: (int(x[id_index]), 1))

   
    print()
    print("###########################")
    print("RDD persisted to disk only")
    print("###########################")
    machine_events.persist(StorageLevel.DISK_ONLY)
    tests(machine_events)


    print()
    print("#################################")
    print("RDD persisted to memory and disk")
    print("#################################")
    machine_events.unpersist()
    machine_events.persist(StorageLevel.MEMORY_AND_DISK)
    tests(machine_events)


    print()
    print("#############################")
    print("RDD persisted to memory only")
    print("#############################")
    machine_events.unpersist()
    machine_events.cache()
    tests(machine_events)
