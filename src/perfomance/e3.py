import os
import time
from pyspark import StorageLevel

import settings
from .tests import *

def e3():
    print("Application level study: Partitioning scheme")
    machine_events_filename = "machine_events/part-00000-of-00001.csv.gz"

    id_index = settings.schema.get_column_for_file(machine_events_filename, "machine ID")

    machine_events = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"], "machine_events/"))
    machine_events = machine_events.map(lambda x : x.split(',')).map(lambda x: (int(x[id_index]), 1))
    machine_events.persist(StorageLevel.DISK_ONLY)
    nb_partitions = machine_events.getNumPartitions()
    
    print("Number of partitions used", nb_partitions)
    print("#######################")
    print("Default Partitioner")
    print("#######################")

    tests(machine_events)

    machine_events.unpersist()
    machine_events = machine_events.partitionBy(nb_partitions)
    machine_events.persist(StorageLevel.DISK_ONLY)

    print("#######################")
    print("Hash Partitioner")
    print("#######################")

    tests(machine_events)
