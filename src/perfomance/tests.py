import time


def tests(rdd):
    start_time = time.time()
    rdd.take(1)
    end_time = time.time()
    print("Map duration: {:.3f} ms".format((end_time-start_time)*1000))

    start_time = time.time()
    rdd.count()
    end_time = time.time()
    print("Count duration: {:.3f} ms".format((end_time-start_time)*1000))

    start_time = time.time()
    rdd.sortByKey().take(1)
    end_time = time.time()
    print("SortByKey duration {:.3f} ms".format((end_time-start_time)*1000))

    start_time = time.time()
    rdd.groupByKey().mapValues(len).take(1)
    end_time = time.time()
    print("GroupByKey duration {:.3f} ms".format((end_time-start_time)*1000))


    start_time = time.time()
    rdd.join(rdd).take(1)
    end_time = time.time()
    print("Join duration {:.3f} ms".format((end_time-start_time)*1000))
