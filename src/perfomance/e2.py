import os
import time
from pyspark import StorageLevel

import settings
from .tests import *

def e2():
    print("Application level study: Number of partitions")
    machine_events_filename = "machine_events/part-00000-of-00001.csv.gz"

    id_index = settings.schema.get_column_for_file(machine_events_filename, "machine ID")

    machine_events = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"], "machine_events/"))
    machine_events = machine_events.map(lambda x : x.split(',')).map(lambda x: (int(x[id_index]), 1))
    machine_events.persist(StorageLevel.DISK_ONLY)
    nb_partitions = machine_events.getNumPartitions()

    partitions = [nb_partitions, 8, 100, 20, 500, 4]
    for p in partitions:
        print()
        print("#########################")
        print("Number of partitions: {}".format(p))
        print("#########################")
        
        if(p!=machine_events.getNumPartitions()):
            machine_events.unpersist()
            machine_events.repartition(p)
            machine_events.persist(StorageLevel.DISK_ONLY)

        tests(machine_events)
