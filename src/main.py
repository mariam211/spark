from pyspark import SparkContext, StorageLevel
from schema import Schema
import argparse
import os, sys
import settings

settings.init()

from analysis.q1 import *
from analysis.q2 import *
from analysis.q3 import *
from analysis.q4 import *
from analysis.q5 import *
from analysis.q6 import *
from analysis.q7 import *
from analysis.q8 import *
from analysis.q9 import *
from analysis.q11 import *
from analysis.q10 import *
from perfomance.e1 import *
from perfomance.e2 import *
from perfomance.e3 import *


def main():

    ######################
    # Handling arguments #
    ######################

    parser = argparse.ArgumentParser(description='Apache Spark Lab on google dataset')
    parser.add_argument("-q", "--question", nargs=1, type=int, choices=range(1, 12), help="Choose the question to run")
    parser.add_argument("-e", "--evaluation", nargs=1, type=int, choices=range(1, 4), help="Choose the performance evaluation to run")
    parser.add_argument("-d", "--dataframe", action="store_true", help="Run with Dataframes instead of RDDs")
    parser.add_argument("-o", "--output", nargs=1, help="Store the output in a csv file")
    parser.add_argument("-c", "--csv", action="store_true", help="Output with csv format")
    parser.add_argument("-g", "--graph", action="store_true", help="Output a graph in pdf format")

    options = parser.parse_args()

    if options.output is None:
        settings.outname = None
    else:
        settings.outname = options.output[0]

    settings.dataframe = options.dataframe
    settings.csv = options.csv
    settings.graph = options.graph



    #################
    # Spark Context #
    #################
    
    settings.sc_conf = SparkConf()
    settings.sc_conf.setMaster("local[*]")\
        .set("spark.driver.cores", "1")\
        .set("spark.driver.maxResultSize", "1g")\
        .set("spark.driver.memory", "2g")\
        .set("spark.driver.memoryOverheadFactor", "0.3")\
        .set("spark.executor.cores", "1")\
        .set("spark.executor.memory", "1g")\
        .set("spark.executor.memoryOverheadFactor", "0.38")\
        .set("spark.memory.fraction", "0.5")\
        .set("spark.memory.offHeap.size", "300m")\
        .set("spark.memory.offHeap.enabled", "true")\
        .set("spark.executor.extraJavaOptions", "-XX:+UseCompressedOops")\
        .set("spark.cleaner.referenceTracking.blocking", "false")\
        .set("spark.dynamicAllocation.enabled", "true")\

    settings.sc = SparkContext(conf=settings.sc_conf)
    settings.sc.setLogLevel("ERROR")


    ###################
    # Building Schema #
    ###################

    # read the input file into an RDD[String]
    settings.schema = Schema(settings.config["SCHEMA_FILE"])


    ####################
    # Calling Question #
    ####################

    if(options.question):
        settings.question = options.question[0]

        if settings.question == 1:
            q1()
        elif settings.question == 2:
            q2()
        elif settings.question == 3:
            q3()
        elif settings.question == 4:
            q4()
        elif settings.question == 5:
            q5()
        elif settings.question == 6:
            q6()
        elif settings.question == 7:
            q7()
        elif settings.question == 8:
            q8()
        elif settings.question == 9:
            q9()
        elif settings.question == 10:
            q10()
        elif settings.question == 11:
            q11()
    elif(options.evaluation):
        settings.evaluation = options.evaluation[0]

        if settings.evaluation == 1:
            e1()
        elif settings.evaluation == 2:
            e2()
        elif settings.evaluation == 3:
            e3()


if __name__ == '__main__':

    main()
