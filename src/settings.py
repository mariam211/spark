# Global variables, shared between modules

import sys, os
from dotenv import load_dotenv, dotenv_values

def init():
    global config
    config = dotenv_values(os.path.join(os.environ["LSDM_HOME"],".env"))
    question = 1
    dataframe = False
    out = sys.stdout
    outname = None
    csv = True
    graph = False
    sc = None # SparkContext
    ss = None # SparkSession
    schema = None
    sc_conf = None
