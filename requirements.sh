#!/bin/bash

lsdm_dir=$(realpath $(dirname $0))

echo -n "Downloading spark and kafka..."
wget https://dlcdn.apache.org/spark/spark-3.3.1/spark-3.3.1-bin-hadoop3.tgz -O ${lsdm_dir}/spark-3.3.1-bin-hadoop3.tgz
wget https://archive.apache.org/dist/kafka/2.7.0/kafka_2.12-2.7.0.tgz -O ${lsdm_dir}/kafka_2.12-2.7.0.tgz
echo "done."

echo -n "Extracting archives..."
tar xzf ${lsdm_dir}/spark-3.3.1-bin-hadoop3.tgz -C ${lsdm_dir}/
tar xzf ${lsdm_dir}/kafka_2.12-2.7.0.tgz -C ${lsdm_dir}/
echo "done."

rm ${lsdm_dir}/spark-3.3.1-bin-hadoop3.tgz
rm ${lsdm_dir}/kafka_2.12-2.7.0.tgz

mkdir ${lsdm_dir}/data/job_events
mkdir ${lsdm_dir}/data/task_usage
mkdir ${lsdm_dir}/data/task_events
mkdir ${lsdm_dir}/data/machine_events
mkdir ${lsdm_dir}/data/checkpoints
mkdir ${lsdm_dir}/results


echo -n "Editing ~/.bashrc..."
cat >> ~/.bashrc<< EOF
export SPARK_HOME="${lsdm_dir}/spark-3.3.1-bin-hadoop3"
export KAFKA_HOME="${lsdm_dir}/kafka_2.12-2.7.0"
export LSDM_HOME="${lsdm_dir}"

export PYTHONPATH="\${SPARK_HOME}/python/:\${PYTHONPATH}"
export PYTHONPATH="\${SPARK_HOME}/python/lib/py4j-0.10.9.5-src.zip:\${PYTHONPATH}"

export PATH="\${SPARK_HOME}/bin:\${KAFKA_HOME}/bin:\${PATH}"

export SPARK_LOCAL_IP="127.0.0.1"
EOF
echo "done."

echo -n "Installing pip dependencies..."
pip3 install kafka-python
pip3 install python-dotenv
pip3 install pyspark
echo "done."
