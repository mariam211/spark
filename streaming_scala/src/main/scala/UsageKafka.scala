/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples.streaming

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd._
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka010._
import org.apache.spark.SparkContext._
import org.apache.spark.streaming.dstream._

/**
 * Consumes messages from one or more topics in Kafka and does wordcount.
 * Usage: DirectKafkaWordCount <brokers> <topics>
 *   <brokers> is a list of one or more Kafka brokers
 *   <groupId> is a consumer group name to consume from topics
 *   <topics> is a list of one or more kafka topics to consume from
 *
 * Example:
 *    $ bin/run-example streaming.DirectKafkaWordCount broker1-host:port,broker2-host:port \
 *    consumer-group topic1,topic2
 */

class Schema(val ssc: StreamingContext, val filename: String) {
    val sc = ssc.sparkContext
    var file = sc.textFile(filename)
    var first_line = file.collect()(0)
    var entries = file.map(x => x.split(","))

    def get_columns(filename: String) = {
        entries.filter(_.contains(filename)).map(x => x(2)).collect().toList
    }

    def get_column_for_file(filename: String, column_name: String) = {
        val file_cols = get_columns(filename)

        if (file_cols.contains(column_name)){
           file_cols.indexOf(column_name)
        } else{
           -1
        }
    }
}

object DirectKafkaWordCount {
  def main(args: Array[String]) {
    if (args.length < 3) {
      System.err.println(s"""
        |Usage: DirectKafkaWordCount <brokers> <groupId> <topics>
        |  <brokers> is a list of one or more Kafka brokers
        |  <groupId> is a consumer group name to consume from topics
        |  <topics> is a list of one or more kafka topics to consume from
        |
        """.stripMargin)
      System.exit(1)
    }

    StreamingExamples.setStreamingLogLevels()

    val Array(brokers, groupId, topics) = args

    // Create context with 2 second batch interval
    val sparkConf = new SparkConf().setAppName("DirectKafkaWordCount")
    val ssc = new StreamingContext(sparkConf, Seconds(5))
    val sc = ssc.sparkContext
    val schema = new Schema(ssc, System.getenv("LSDM_HOME")+"/data/schema.csv")
    ssc.checkpoint(System.getenv("LSDM_HOME")+"/data/checkpoints")
    val task_usage_filename = "task_usage/part-?????-of-?????.csv.gz"
    val cols =  schema.get_columns(task_usage_filename)
    val time_index = schema.get_column_for_file(task_usage_filename, "time")
    val cpu_usage_index = schema.get_column_for_file(task_usage_filename, "sampled CPU usage")
    
    // Create direct kafka stream with brokers and topics
    val topicsSet = topics.split(",").toSet
    val kafkaParams = Map[String, Object](
      ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> brokers,
      ConsumerConfig.GROUP_ID_CONFIG -> groupId,
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer],
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer])
    val messages = KafkaUtils.createDirectStream[String, String](
      ssc,
      LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](topicsSet, kafkaParams))


    //extract data
    var lines = messages.map(_.value)
    var data = lines.map(_.split(","))
    data = data.filter(x => x(cpu_usage_index)!=None && x(cpu_usage_index)!=" ")

    //count number of tasks by window
    var countStream: DStream[Long] = data.countByWindow(Seconds(20), Seconds(5))
    //compute total CPU usage
    var totalStream: DStream[Float] = data.map(x => x(cpu_usage_index).toFloat).reduceByWindow((x: Float, y: Float) => x+y, (x: Float, y: Float) => x-y, Seconds(20), Seconds(5))
    //Compute number of occurrences for each CPU usage in a window
    var usageStream: DStream[(Float, Long)] = data.map(x => x(cpu_usage_index).toFloat).countByValueAndWindow(Seconds(20), Seconds(5))
    //Sort usage stream from CPU usage with highest frequency to least ones
    var sortedUsageStream: DStream[(Float, Long)] = usageStream.transform((usageRdd: RDD[(Float, Long)]) => usageRdd.sortBy(x => x._2, ascending = false))
    
    sortedUsageStream.print(3)
    countStream.print()
    totalStream.print()

    // Start the computation
    //start streaming
    ssc.start()
    ssc.awaitTermination()
  }
}
