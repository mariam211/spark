#!/bin/bash


${KAFKA_HOME}/bin/kafka-topics.sh  --create --bootstrap-server localhost:9092 \
--replication-factor 1 --partitions 2 \
--topic event

${KAFKA_HOME}/bin/kafka-topics.sh  --create --bootstrap-server localhost:9092 \
--replication-factor 1 --partitions 2 \
--topic usage

${KAFKA_HOME}/bin/kafka-topics.sh  --create --bootstrap-server localhost:9092 \
--replication-factor 1 --partitions 2 \
--topic mem
