from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import split
import argparse, os, socket, sched, sys, time

sys.path.append(os.path.join(os.environ["LSDM_HOME"],"src/"))

from schema import Schema
import settings

def consume_event(servers="127.0.0.1:9092"):
	df = settings.ss.readStream\
		.format("kafka")\
		.option("kafka.bootstrap.servers", servers)\
		.option("subscribe", "event")\
		.load()

	def split_data(df):
		mid = settings.schema.get_field_nb(\
			"machine_events/part-00000-of-00001.csv.gz",\
			"machine ID")
		cpus = settings.schema.get_field_nb(\
			"machine_events/part-00000-of-00001.csv.gz",\
			"CPUs")
		return df\
			.withColumn("machine ID", split(df.value, ',')[mid])\
			.withColumn("CPUs", split(df.value, ',')[cpus])

	query = df.selectExpr("CAST(value AS STRING)")\
		.transform(split_data)\
		.select("machine ID", "CPUs")\
		.dropDuplicates()\
		.groupBy("CPUs")\
		.agg({"machine ID":"count"})\
		.writeStream\
		.outputMode("complete")\
		.format("console")\
		.start()\
		.awaitTermination()
