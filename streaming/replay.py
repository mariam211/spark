# Replayer
#
# This program takes a google file pattern as input and replays the data.

from pyspark import SparkContext, SparkConf, StorageLevel
from pyspark.streaming import StreamingContext
from kafka import KafkaProducer

from threading import Thread
import argparse, os, socket, sched, sys, time
sys.path.append(os.path.join(os.environ["LSDM_HOME"],"src/"))

from schema import Schema
import settings
settings.init()

existing_topics = {"event": {"file_pattern": "machine_events/part-00000-of-00001.csv.gz", "file_name": "machine_events/", "time_column": "time"}, \
                   "usage": {"file_pattern": "task_usage/part-?????-of-?????.csv.gz", "file_name": "task_usage/", "time_column": "start time"}, \
                   "mem": {"file_pattern": "task_usage/part-?????-of-?????.csv.gz", "file_name": "task_usage/", "time_column": "start time"}\
                  }

def publish(entry, topic, producer):
    entry = ",".join(entry)
    producer.send(topic, value=bytes(str(entry), 'utf-8'))

def prepare_replay(file_pattern, file_name, topic, time_column="time"):
    time_index = settings.schema.get_field_nb(file_pattern, time_column)
    data = settings.sc.textFile(os.path.join(settings.config["DATA_PATH"],file_name))
    data = data.map(lambda x: x.split(','))
    data.persist(StorageLevel.DISK_ONLY)

    # Convert microseconds to seconds, sort by timestamp
    data = data.map(lambda x: (int(x[time_index])*0.000001, x)).sortByKey()
    MAX_INT = (pow(2,63)-1)*0.000001
    max_time = data.filter(lambda x: x[0]!=MAX_INT).max()[0]

    # We could consider removing the 600 seconds shift
    # But we keep it and replay the events much faster
    #data = data.map(lambda x: (x[0]-600 if x[0]>0 else 0, x[1]))

    # We bring the post-trace events 600 seconds after the last in-trace event
    data = data.map(lambda x: (x[0] if x[0]!=MAX_INT else max_time+600, x[1]))

    return data


def get_speed(speed):
    if speed.lower() == "fast":
        return 0.000001
    elif speed.lower() == "normal":
        return 1
    elif speed.lower() == "slow":
        return 100000
    else:
        print("Unknown speed. Using default fast speed")
        return 0.000001


def replay(kafka_host, file_pattern, file_name, topic="event", time_column="time", speed="fast"):
    data = prepare_replay(file_pattern, file_name, topic, time_column)
    producer = KafkaProducer(bootstrap_servers=[kafka_host])

    # Schedule the events
    scheduler = sched.scheduler(time.time, time.sleep)
    for entry in data.collect():
        # Note: we replay the events 1000 times faster (one millisecond is one second)
        scheduler.enter(entry[0]*get_speed(speed), 1, publish, (entry[1], topic, producer))

    print('kafka-replayer: Start replaying for topic', topic)
    run_replay(producer, scheduler)
    print('kafka-replayer: Finished replaying for topic', topic)


def run_replay(producer, scheduler):
    # Start playing the events
    try:
        scheduler.run()
    except KeyboardInterrupt:
        producer.close()


if __name__ == '__main__':
    kafka_host = "localhost:9092"
    parser = argparse.ArgumentParser(description='Apache Spark Lab on google dataset')
    parser.add_argument("-t", "--topics", nargs='*', help="Choose the topic to stream")
    parser.add_argument("-k", "--kafka", nargs=1, help="Specify kafka host")

    options = parser.parse_args()
    topics = options.topics

    if topics and not set(topics).issubset(set(existing_topics.keys())):
        print("Invalid topic. Exiting")
        sys.exit()

    if(options.kafka):
        kafka_host = options.kafka

    settings.sc_conf = SparkConf()
    settings.sc_conf.setMaster("local[*]")\
        .set("spark.driver.cores", "1")\
        .set("spark.driver.maxResultSize", "1g")\
        .set("spark.driver.memory", "2g")\
        .set("spark.driver.memoryOverheadFactor", "0.3")\
        .set("spark.executor.cores", "1")\
        .set("spark.executor.memory", "1g")\
        .set("spark.executor.memoryOverheadFactor", "0.38")\
        .set("spark.memory.fraction", "0.5")\
        .set("spark.memory.offHeap.size", "300m")\
        .set("spark.memory.offHeap.enabled", "true")\
        .set("spark.executor.extraJavaOptions", "-XX:+UseCompressedOops")\
        .set("spark.cleaner.referenceTracking.blocking", "false")\
        .set("spark.dynamicAllocation.enabled", "true")\

    settings.sc = SparkContext(conf=settings.sc_conf)
    settings.sc.setLogLevel("ERROR")

    settings.schema = Schema(settings.config["SCHEMA_FILE"])

    if not topics:
        topics = existing_topics.keys()

    threads = []

    for topic in topics:
        thread = Thread(target = replay, args=(kafka_host, existing_topics[topic]["file_pattern"], existing_topics[topic]["file_name"], topic, existing_topics[topic]["time_column"]))
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()
