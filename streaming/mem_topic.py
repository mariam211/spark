from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import split
from pyspark.sql.functions import window
from pyspark.sql.functions import array_sort
from pyspark.sql.functions import collect_set
from pyspark.sql.functions import first
from pyspark.sql.functions import to_timestamp
import argparse, os, socket, sched, sys, time

sys.path.append(os.path.join(os.environ["LSDM_HOME"],"src/"))

from schema import Schema
import settings

def consume_mem(servers="127.0.0.1:9092"):
	df = settings.ss.readStream\
		.format("kafka")\
		.option("kafka.bootstrap.servers", servers)\
		.option("subscribe", "mem")\
		.load()

	def split_data(df):
		tid = settings.schema.get_field_nb(\
			"task_usage/part-?????-of-?????.csv.gz",\
			"task index")
		mem = settings.schema.get_field_nb(\
			"task_usage/part-?????-of-?????.csv.gz",\
			"canonical memory usage")
		time = settings.schema.get_field_nb(\
			"task_usage/part-?????-of-?????.csv.gz",\
			"start time")
		return df\
			.withColumn("tid", split(df.value, ',')[tid])\
			.withColumn("memusage", split(df.value, ',')[mem])\
			.withColumn("starttime", to_timestamp(split(df.value, ',')[time]))

	w = window("starttime", "300 seconds", "250 seconds")

	format = df.selectExpr("CAST(value AS STRING) as value", "timestamp")\
		.transform(split_data)\
		.selectExpr(\
			"CAST(tid AS INT) as tid",\
			"CAST(memusage AS FLOAT) as memusage",\
			"CAST(starttime AS TIMESTAMP) as starttime",\
			"CAST(timestamp AS TIMESTAMP) as timestamp"\
		)

	query = format.writeStream\
		.outputMode("update")\
		.format("console")\
		.start()\
		.awaitTermination()

	#query = format.groupBy(w)\
	#	.max("memusage")\
	#	.writeStream\
	#	.outputMode("complete")\
	#	.format("console")\
	#	.start()\
	#	.awaitTermination()

		#.agg(array_sort(collect_set("canonical memory usage")).alias('canonical memory usage'))\


		#.select(window("start time", "1 second", "500 milliseconds"), "task index", "canonical memory usage")\
		#.select("task index", "canonical memory usage")\
                #.sort("canonical memory usage")\
                #.writeStream\
                #.outputMode("update")\
                #.format("console")\
                #.start()\
                #.awaitTermination()
