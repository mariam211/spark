#!/bin/bash

if [ $# -ne 1 ]; then
	echo "usage: $0 <topic>"
	exit 1
fi

${SPARK_HOME}/bin/spark-submit --packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.3.1 ${LSDM_HOME}/streaming/consumer.py -t $1
