from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import split

import argparse, os, socket, sched, sys, time

from event_topic import consume_event
from mem_topic import consume_mem

sys.path.append(os.path.join(os.environ["LSDM_HOME"],"src/"))

from schema import Schema
import settings
settings.init()

def consume(topic, servers="127.0.0.1:9092"):
	if topic == "event":
		consume_event(servers=servers)
	elif topic == "mem":
		consume_mem(servers=servers)
	else:
		print("Unknown topic. No matching consumer was found.")

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Apache Spark Lab on google dataset')
	parser.add_argument("-t", "--topic", nargs=1, help="Set the topic to stream", required=True)
	parser.add_argument("-s", "--servers", nargs=1, help="Set kafka servers")

	options = parser.parse_args()

	topic = options.topic[0]
	servers = "127.0.0.1:9092"
	if options.servers:
		servers = options.servers

	settings.sc = SparkContext("local[*]")
	settings.sc.setLogLevel("ERROR")
	settings.schema = Schema(settings.config["SCHEMA_FILE"])
	settings.ss = SparkSession(settings.sc)

	consume(topic, servers=servers)
